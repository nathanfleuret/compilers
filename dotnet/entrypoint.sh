#!/bin/bash
if [ $# = 0 ]; then
   echo "no code received"
   exit
fi
printf "$1" > "Program.cs"
if [ $# = 1 ]; then
    ulimit -s 1000
    timeout -s SIGTERM 15 dotnet run
    exit
fi
shift
for argument in "${@}"
do
    ulimit -s 1000
    timeout -s SIGTERM 15 dotnet run $argument
done