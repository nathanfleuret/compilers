#!/bin/bash
if [ $# = 0 ]; then
   echo "no code received"
   exit
fi
printf "$1" > "Main.java"
javac Main.java
if [ $# = 1 ]; then    
    ulimit -s 1000
    timeout -s SIGTERM 15 java Main.java
    exit
fi
shift
for argument in "${@}"
do    
    ulimit -s 1000
    timeout -s SIGTERM 15 java Main.java $argument
done