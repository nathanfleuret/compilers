#!/bin/bash
if [ $# = 0 ]; then
   echo "no code received"
   exit
fi
printf "$1" > "main.py"
if [ $# = 1 ]; then    
    ulimit -s 1000
    timeout -s SIGTERM 15 python3 main.py
    exit
fi
shift
for argument in "${@}"
do    
    ulimit -s 1000
    timeout -s SIGTERM 15 python3 main.py $argument
done